<!-- Created Abdul Haris Dwi Utomo 2018-->
<!-- FILE UPLOAD-->
<!--
Dalam pembuatan aplikasi berbasis web, mengupload file merupakan sesuatu yang
sangat di perlukan dalam php, syntax untuk mengupload file pun sudah di sediakan ... ada
sedikit perbedaan antara form yang biasa dengan form yang digunakan untuk
mengupload file. Terdapat satu atribut tambahan untuk form tersebut, kita coba akan buat
terlebih dahulu form yang digunakan untuk mengupload file nantinya ...


buat file dengan nama formupload.php
-->

<html>
<head>
<title>Form Upload</title>
</head>
<body>
<form enctype="multipart/form-data" action="upload.php" method="post">
<p>
<!-- MAX_FILE_SIZE digunakan untuk membatasi size/ukuran file yang akan
di upload -->
<input type="hidden" name="MAX_FILE_SIZE" value="102400" />
<input type="file" name="file" /><br/><!--input untuk upload file-->
<input type="submit" value="upload!" />
</p>
</form>
</body>
</html>

<!-- buat file upload.php untuk memprosesnya-->

<?php
// ketika filenya dikirim ...
if ( isset( $_FILES['file'] ) ) {

echo "name: ". $_FILES['file']['name']."<br />";
echo "size: ". $_FILES['file']['size']." bytes<br />";
echo "temp name: ".$_FILES['file']['tmp_name']."<br />";
echo "type: ". $_FILES['file']['type']."<br />";
echo "error: ". $_FILES['file']['error']."<br />";


// jila file berupa gambar
if ( $_FILES['file']['type'] == "image/jpeg" ) {
	// kemudian ambil letak file temporary,
	// ini dilakukan karena ketika file itu di upload
	// server php dalam hal ini apache secara otomatis meletakkannya
	// sebagai temporary file
	$source = $_FILES['file']['tmp_name'];
	// kemudian buat target atau akan di letakkan di mana file tersebut
	$target = "upload/".$_FILES['file']['name'];
	// setelah file temporarinya sudah di definisikan
	// dan kemudian file tujuan akhir, atau akan di letakkan di mana file
	// yang akan di upload tadi
	// selanjutnya adalah memindahkan file temporari tersebut ke dalam
	// folder tujuan, yakni folder upload
	move_uploaded_file( $source, $target );
	// untuk mendapatkan ukuran atau size dari image yang di upload ...
	$size = getImageSize( $target );
	// kemudian tampilkan image yang telah di upload tersebut ke dalam
	// halaman yang saat ini di akses ...
	// dalam hal ini file tersebut adalah upload.php
	$imageupload = "<p><img width=\"$size[0]\" height=\"$size[1]\"
	" . "src=\"$target\" alt=\"uploaded image\" /></p>";
	echo $imageupload;
}//end jika file adalah gambar

}
?>