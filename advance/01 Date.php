<!-- DATE -->
<!-- Created By Abdul Haris Dwi Utomo-->
<!--
Penggunaan Date() pada php berfungsi untuk menampilkan tanggal dan waktu seusai dengan tanggal dan sistem komputer anda

syntaxnya seperti dibawah ini:
date(format,timestamp)
ket:
format	Wajib diisi. Spesifik format pada timestamp
timestamp	Optional(tidak wajib diisi). spesifik timestamp, defaultnya adalah tanggal dan waktu hari ini
-->

<?php
echo "Hari ini " . date("Y/m/d") . "<br>";
echo "Hari ini " . date("Y.m.d") . "<br>";
echo "Hari ini " . date("Y-m-d") . "<br>";
echo "Hari ini " . date("l");
?>

<!--- contoh lain html dan php menggunakan fungsi date-->
&copy; 2010-<?php echo date("Y");?>


<!-- contoh lain memunculkan waktu-->
<?php
echo "The time is " . date("h:i:sa");
?>

<!-- jika waktu pada server anda tidak sesuai anda bisa memformatnya dengan lokasi anda saat ini dengan cara-->

<?php
date_default_timezone_set('Asia/Jakarta');
echo "The time is " . date("h:i:sa");
?>

<!-- membuat tanggal dari string yang sudah ada dan memformatnya dengan strtotime-->

<?php
$d=strtotime("2018-05-27");
echo date("Y-m-d", $d) . "<br>";

$d=strtotime("tomorrow");
echo date("Y-m-d h:i:sa", $d) . "<br>";

$d=strtotime("next Saturday");
echo date("Y-m-d h:i:sa", $d) . "<br>";

$d=strtotime("+3 Months");
echo date("Y-m-d h:i:sa", $d) . "<br>";
?>
<!-- mktime tidak dibahas disini mengingat waktu yang sangat sempit, tapi kalian bisa mencari tau tentang mktime di website2 sperti w3schools dll-->