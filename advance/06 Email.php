<!--EMAIL-->
<!--Created by Abdul Haris Dwi Utomo @2018-->
<!--untuk mengirim email melalui html fungsi php yang perlu kita panggil adalah
	mail(emailtujuan, judul, isipesan, header);
-->
<?php
$to = "harizu.sekai@gmail.com";
$subject = "Email Contoh";
$message = "Ini adalah email Contoh ... ";
$header = "From: <haris@digitalindo.co.id>";
// kemudian untuk mengirim email
$sent = mail($to, $subject, $message, $header);
if($sent)
{
echo "Email contoh sudah terkirim...";
}
else
{
echo "Gagal kirim email!";
}

//namun script ini perlu settingan lebih mendalam jika di jalankan pada localhost
// jika anda jalankan pada server yang sudah ada mail server script ini cukup berfungsi apabila ingin mengkostumisasi script email ini tinggal pada pengaturan SMTP sesuaikan dengan server masing2.

//namun jika kalian ingin coba pada server local anda bisa googling dengan kata kunci mengirim email dengan php localhost
?>