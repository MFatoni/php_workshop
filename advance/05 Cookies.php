<!--SESSIONS-->
<!--Created by Abdul Haris Dwi Utomo @2018-->
<!--
	Jika session itu akan berakhir ketika browser ditutup, namun jika cookie itu akan berakhir
pada waktu yang ditentukan oleh web developer sebelumnya. Contoh penggunaan cookie
adalah seperti ini :
-->

<?php
//pertama kita buat terlebih dulu file cookie1.php

// lalu di sini kita siapkan variable untuk cookienya
$isicookie = "ini adalah isi dari cookie";
// kemudian kita buat cookienya dengan lama waktu 1 jam misalnya ...
// di hitung dengan menggunakan satuan waktu detik
setcookie("cookie1", $isicookie, time()+3600);
// untuk dapat mengakses cookie anda dapat menggunakan sintax
echo $_COOKIE["cookie1"];
?>

<!--mengakses cookie di halaman lain other_cookie.php-->

<?php
// kemudian langsung akses variable cookienya
echo $_COOKIE["cookie1"];
?>

<!-- menghapus cookie-->
<?php
// hapus cookie yang ada dengan unset
unset($_COOKIE['cookie1']);
echo $_COOKIE['cookie1'];
?>