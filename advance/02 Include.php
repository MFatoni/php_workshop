<!-- Created Abdul Haris Dwi Utomo 2018-->
<!-- INCLUDE DAN REQUIRE UNTUK EFISIENSI CODING-->
<!--
Fungsi include dan require berfungsi untuk menjalankan banyak file secara
berbarengan dengan caraa memasukkan file lain kedalam suatu file tertentu.
include "namafile.php";
require "namafile.php";

include() include_once() require() require_once()

INCLUDE
Fungsi Include digunakan di PHP ketika kita ingin menyertakan suatu file kedalam proses yang saat ini sedang berjalan. Dibutuhkan satu argumen yang akan menjadi string ke path file yang anda ingin sertakan.

INCLUDE_ONCE
Fungsi Include_once hampir sama dengan fungsi Inlcude, namun akan membatasi file yang hanya akan digunakan 1 kali.

Require
Fungsi Require bekerja seperti fungsi Include, namun jika file tersebut tidak ditemukan maka akan melemparkan PHP Error. Fungsi ini dibutuhkan untuk aplikasi yang bekerja dengan benar.

Require_once
merupakan kombinasi fungsi Require dan Include_once. Ini akan memastikan bahwa file ada sebelum menambahkannya ke halaman, jika tidak ada maka akan memunculkan Fatal Error. Plus itu akan memastikan bahwa file tersebut hanya akan digunakan sekali pada halaman web.
-->

<?php
// buatfile latihaninclude.php
$cuaca = "hujan";
?>

<!--di file yang berbeda buatlah seperti dibawha ini -->
<?php
include "latihaninclude.php";
if ($cuaca == "cerah")
{
echo "Latihan bola di GOR";
}
else
{
echo "Bikin mie ramen";
}
?>