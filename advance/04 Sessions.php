<!--SESSIONS-->
<!--Created by Abdul Haris Dwi Utomo-->
<!--

	Session merupakan variable global yang jika diregistrasikan atau didefinisikan
maka variable tersebut dapat di akses di halaman manapun. ketika kita meregistrasikan
session, maka php akan kepada browser untuk menyimpan session yang ada, sehingga
session disimpan oleh browser sementara mirip dengan pengandaian sebuah tiket masuk.
setiap halaman php yang membutuhkan adanya login akan mengecek apakah variable
sessionnya sudah diregistrasikan. jika sudah maka, halaman php tersebut akan
mempersilahkan user dapat mengaksesnya...
Oke, karena session memiliki tugas khusus dan session bertugas sebagai variable
global yang dapat di akses di halaman mana saja, sehingga session pun membutuhkan
fungsi tersendiri untuk dapat menjalankannya.
Standarnya membuat session adalah seperti

-->

<?php
//buat file dengan nama session_reg_first.php
// untuk membuat session, diperlukan fungsi khusus yang dapat
// memproduksi session
// yakni, dan jangan lupa untuk meletakkan sessio_start di baris paling
// awal setelah <?php
session_start();
$_SESSION['first'] = "saya adalah session";
echo "Anda telah meregistrasikan session berisi '<strong>".
$_SESSION['first'] . "</strong>'";

?>
<br />
<a href="other_page.php">klik di sini untuk pindah halaman</a>

<!--buat file dengan nama other_page.php-->

	<?php
session_start();
echo "Anda telah membuat session di halaman sebelumnya, berisi
'<strong>" . $_SESSION['first']."</strong>'";

?>

<!--buat file dengan nama random.php-->
<?php
// jangan lupa untuk memasukkan fungsi session_start setiap akan
// meregistrasi atau mengakses session
session_start();
// kita coba akses variable session yang telah di set
echo "isi dari session yang aktif ".$_SESSION['first'];
?>

<!-- Menghapus session buat file bernama logout.php-->
<?php
session_start();
unset($_SESSION['first']);
echo "Isi ".$_SESSION['first']." adalah ... = " . $_SESSION['first'];
// jika ingin memusnahkan semua session yang ada
// anda dapat menggunakan session_destroy
// biasanya hal ini digunakan ketika proses logout terjadi
// semua session yang ada benar-benar di hapus
// penggunaanya adalah seperti ini
session_destroy();
echo "<br />Semua session telah di hapus ...";
?>