<!--Perulangan (looping)-->
<!-- Created By Abdul Haris Dwi Utomo 2018 -->

<!--
Perulangan atau looping di gunakan untuk menjalankan program secara berulang hanya dengan sedikit syntax
perulangan ada 3 metode menggunakan for,while atau do while

contoh penggunaan looping menggunakan for menggunakan syntax seperti ini
for(nilaiawal;nilaibatas;operator increament/decrement)
{
	pernyataan yang akan di proses	
}

contoh perulangan menggunakan for
-->
<?php
//contoh 1
for($x=1;$x<=100;$x++){
	echo "Angka $x<br/>";
}
//contoh 2
for($x=1;$x<=100;$x+=2){
	echo "Angka $x<br/>";
}
//contoh 3
for($x=100;$x>=1;$x--){
	echo "Angka $x<br/>";
}
?>

<!-- 
while
perulangan menggunakan while tidak jauh beda dengan for yang membedakan hanya penempatan nilai awalnya
-->
<?php
$x=1;
while ($x <=100)
{
	echo "Angka $x<br/>";
	$x++; //bagian ini wajib ada jika tidak ada program akan terus menerus berjalan
}
?>
<!--
	do whie
	penggunaan do while sama dengan while yang membedakan hanya penempatan whilenya dan ditambah semicolon(;)
	-->

<?php
$x=1;
do 
{
	echo "Angka $x<br/>";
	$x++; //bagian ini wajib ada jika tidak ada program akan terus menerus berjalan
}

while($x <=100);
?>