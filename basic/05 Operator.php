<!--Operator-->
<!-- Created By Abdul Haris Dwi Utomo 2018 -->
<?php
//Ada beberapa jenis operator yang berguna dalam pemograman PHP diantaranya
//Ada operator matematika, assignment,
//perbandingan, logika, increment, decrement di bahas pada materi selanjutnya.

//Contoh Operator Mematika
$pengurangan=1000-500;
$pertambahan=2000+1000;
$perkalian=1000*5;
$pembagian=1000/4;
$hasilbagi= 10%2;

echo "1000 - 500 = $pengurangan<br/>"; 
echo "2000 + 1000 = $pertambahan<br/>";
echo "1000 x 5 = $perkalian<br/>";
echo "1000 : 4 = $pembagian<br/>";
echo "10 % 2 = $hasilbagi<br/>";

// pengurangan diwakili dengan tanda "-"
// penjumlahan diwakili dengan tanda "+"
// perkalian diwakili dengan tanda "*"
// pembagian diwakili dengan tanda  "/"
// sisa hasil bagi diwakili dengan tanda "%"
//======================================================================//
//operator assignment(penempatan/replacing)
//operator assignment berguna untuk memasukan nilai kedalam suatu variable
$panjang=10; //contoh assignment
$lebar=8; //contoh assignment
$luas=$panjang * $lebar; // contoh assingment juga

//tanda '='' mewakili assignment
//ada beberapa tambahan yang mewakili assignment contohnya '+='
$tambahdua=0;
$tambahdua +=2;
echo $tambahdua."<br/>";

//hal tesebut sama dengan pernyataan $tambahdua=$tambahdua + 2
//hal tersebut juga berlaku untuk '-=','*=','/='

//contoh assingment
$a=50;
$b=40;
$c=30;
$d=20;

echo "Hasil dari 50+2 adalah " . $a+=2;
echo "Hasil dari 40-2 adalah " . $b-=2;
echo "Hasil dari 30x2 adalah " . $c*=2;
echo "Hasil dari 20:2 adalah " . $d/=2;

//hal tersebut sama dengan $a=$a+2; dst..
//=================================================================//
//tanda titik tersbut untuk melanjutkan sebuah kalimat
//contoh
//echo "Saya" . "Belajar" . "PHP";
//atau
//echo "Saya" , "Belajar" , "PHP";

//jadi penulisan nya bisa 3 opsi
//echo "Nilai a adalah $a";
//echo "Nilai a adalah".$a;
//echo "Nilai a adalah",$a;

//======================================================//
//Untuk Operator Perbandingan kita akan membahasnya pada materi perbandingan atau if else
//untuk operator increment dan decrement akan dijelaskan pada bagian perulangan

?>