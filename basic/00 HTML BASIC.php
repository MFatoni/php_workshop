<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
</head>
<body>

<h1>This is a Heading</h1>
<p>This is a paragraph.</p>

</body>
</html>


















<h2>Kumpulan Tag pada HTML</h2>
<p>Karena banyak sekali Tag dan atribut pada HTML, saya akan bantu untuk mendefinisikan kegunaan Macam- macam Tag secara satu persatu, pada tabel dibawah ini:</p>
<table border="1">
<tbody>
<tr>
<th colspan="2">Tag</th>
<th>Penjelasan</th>
</tr>
<tr>
<td></td>
<td>&lt;!--...--&gt;</td>
<td>Mendefinisikan Komentar</td>
</tr>
<tr>
<td></td>
<td>&lt;!DOCTYPE&gt;</td>
<td>Mendefinisikan tipe atau jenis dokumen</td>
</tr>
<tr>
<td></td>
<td>&lt;a&gt;</td>
<td>Mendefinisikan hyperlink</td>
</tr>
<tr>
<td></td>
<td>&lt;abbr&gt;</td>
<td>Mendefinisikan sebuah singkatan atau akronim</td>
</tr>
<tr>
<td></td>
<td>&lt;acronym&gt;</td>
<td><span class="deprecated">Tag ini tidak didukung pada HTML5. Gunakan &lt;abbr&gt; sebagai penggantinya pada HTML5.
</span>Mendefinisikan akronim</td>
</tr>
<tr>
<td></td>
<td>&lt;address&gt;</td>
<td>Mendefinisikan informasi kontak penulis atau pemilik dokumen</td>
</tr>
<tr>
<td></td>
<td>&lt;applet&gt;</td>
<td><span class="deprecated">Tag ini tidak didukung pada HTML5. Gunakan &lt;embed&gt; atau &lt;object&gt; sebagai penggantinya pada HTML5.
</span>Mendefinisikan embedded applet</td>
</tr>
<tr>
<td></td>
<td>&lt;area&gt;</td>
<td>Mendefinisikan sebuah area didalam image-map</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;article&gt;</td>
<td>Mendefinisikan sebuah artikel</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;aside&gt;</td>
<td>Mendefinisikan selain dari konten halaman</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;audio&gt;</td>
<td>Mendefinisikan konten suara</td>
</tr>
<tr>
<td></td>
<td>&lt;b&gt;</td>
<td>Mendefinisikan teks bold atau cetak tebal</td>
</tr>
<tr>
<td></td>
<td>&lt;base&gt;</td>
<td>Menentukan basis URL atau target untuk semua URL relatif dalam dokumen</td>
</tr>
<tr>
<td></td>
<td>&lt;basefont&gt;</td>
<td><span class="deprecated">Tag ini tidak didukung pada HTML5. Gunakan CSS sebagai penggantinya pada HTML5.
</span>Menentukan warna default, ukuran, dan font untuk semua teks dalam dokumen</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;bdi&gt;</td>
<td>Isolat bagian teks yang mungkin diformat dalam arah yang berbeda dari teks lain di luar itu sendiri</td>
</tr>
<tr>
<td></td>
<td>&lt;bdo&gt;</td>
<td>Mengganti arah teks saat ini</td>
</tr>
<tr>
<td></td>
<td>&lt;big&gt;</td>
<td><span class="deprecated">Tag ini tidak didukung pada HTML5. Gunakan CSS sebagai penggantinya pada HTML5.
</span>Mendefinisikan teks besar</td>
</tr>
<tr>
<td></td>
<td>&lt;blockquote&gt;</td>
<td>Mendefinisikan bagian yang dikutip dari sumber lain</td>
</tr>
<tr>
<td></td>
<td>&lt;body&gt;</td>
<td>Mendefinisikan tubuh atau badan dokumen</td>
</tr>
<tr>
<td></td>
<td>&lt;br&gt;</td>
<td>Mendefinisikan satu baris tunggal atau sama dengan fungsi enter</td>
</tr>
<tr>
<td></td>
<td>&lt;button&gt;</td>
<td>Mendefinisikan sebuah tombol</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;canvas&gt;</td>
<td>Digunakan untuk menggambar grafik, dengan cepat, melalui scripting (biasanya JavaScript)</td>
</tr>
<tr>
<td></td>
<td>&lt;caption&gt;</td>
<td>Mendefinisikan caption dari sebuah tabel</td>
</tr>
<tr>
<td></td>
<td>&lt;center&gt;</td>
<td><span class="deprecated">Tag ini tidak didukung pada HTML5. Gunakan CSS sebagai penggantinya pada HTML5.
</span>Mendefinisikan teks yang posisinya berada ditengah</td>
</tr>
<tr>
<td></td>
<td>&lt;cite&gt;</td>
<td>Mendefinisikan Judul karya</td>
</tr>
<tr>
<td></td>
<td>&lt;code&gt;</td>
<td>Mendefinisikan bagian dari code pada komputer</td>
</tr>
<tr>
<td></td>
<td>&lt;col&gt;</td>
<td>Menentukan sifat kolom untuk setiap kolom dalam &lt;colgroup&gt; element</td>
</tr>
<tr>
<td></td>
<td>&lt;colgroup&gt;</td>
<td>Menentukan kelompok dari satu atau lebih kolom dalam sebuah tabel untuk memformat</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;datalist&gt;</td>
<td>Menentukan daftar pilihan yang telah ditentukan untuk kontrol input</td>
</tr>
<tr>
<td></td>
<td>&lt;dd&gt;</td>
<td>Mendefinisikan deskripsi / nilai istilah dalam daftar deskripsi</td>
</tr>
<tr>
<td></td>
<td>&lt;del&gt;</td>
<td>Mendefinisikan teks yang telah dihapus dari dokumen</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;details&gt;</td>
<td>Mendefinisikan rincian tambahan bahwa pengguna dapat melihat atau menyembunyikan</td>
</tr>
<tr>
<td></td>
<td>&lt;dfn&gt;</td>
<td>Merupakan contoh mendefinisikan istilah</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;dialog&gt;</td>
<td>Mendefinisikan sebuah kotak dialog atau jendela</td>
</tr>
<tr>
<td></td>
<td>&lt;dir&gt;</td>
<td><span class="deprecated">Tag ini tidak didukung pada HTML5. Gunakan &lt;ul&gt; sebagai penggantinya pada HTML5.
</span>Mendefinisikan daftar direktori</td>
</tr>
<tr>
<td></td>
<td>&lt;div&gt;</td>
<td>Mendefinisikan sebuah bagian dalam sebuah dokumen</td>
</tr>
<tr>
<td></td>
<td>&lt;dl&gt;</td>
<td>Mendefinisikan daftar deskripsi</td>
</tr>
<tr>
<td></td>
<td>&lt;dt&gt;</td>
<td>Mendefinisikan istilah / nama dalam daftar deskripsi</td>
</tr>
<tr>
<td></td>
<td>&lt;em&gt;</td>
<td>Mendefinisikan menekankan teks</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;embed&gt;</td>
<td>Mendefinisikan sebuah wadah untuk eksternal (non-HTML) aplikasi</td>
</tr>
<tr>
<td></td>
<td>&lt;fieldset&gt;</td>
<td>Sebauh Grup untuk pengelompokan dalam form</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;figcaption&gt;</td>
<td>Mendefinisikan sebuah caption untuk &lt;figure&gt; element</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;figure&gt;</td>
<td>Menentukan konten mandiri</td>
</tr>
<tr>
<td></td>
<td>&lt;font&gt;</td>
<td><span class="deprecated">Tag ini tidak didukung pada HTML5. Gunakan CSS sebagai penggantinya pada HTML5.
</span>Mendefinisikan font, warna, dan ukuran dari sebuah teks</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;footer&gt;</td>
<td>Mendefinisikan sebuah footer dari sebuah dokumen atau section</td>
</tr>
<tr>
<td></td>
<td>&lt;form&gt;</td>
<td>Mendefinisikan sebuah form HTML untuk input pengguna</td>
</tr>
<tr>
<td></td>
<td>&lt;frame&gt;</td>
<td><span class="deprecated">Tag ini tidak didukung pada HTML5.
</span>Mendefinisikan sebuah jendela (frame) dalam sebuah frameset</td>
</tr>
<tr>
<td></td>
<td>&lt;frameset&gt;</td>
<td><span class="deprecated">Tag ini tidak didukung pada HTML5.
</span>Mendefinisikan sebuah satu set frames</td>
</tr>
<tr>
<td></td>
<td>&lt;h1&gt; sampai &lt;h6&gt;</td>
<td>Mendefinisikan headings pada HTML</td>
</tr>
<tr>
<td></td>
<td>&lt;head&gt;</td>
<td>Mendefinisikan informasi tentang dokumen</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;header&gt;</td>
<td>Mendefinisikan sebuah header untuk dokumen atau bagian dokumen</td>
</tr>
<tr>
<td></td>
<td>&lt;hr&gt;</td>
<td>Mendefinisikan perubahan tematik dalam konten atau memberikan garis pada dokumen</td>
</tr>
<tr>
<td></td>
<td>&lt;html&gt;</td>
<td>Mendefinisikan root dari sebuah dokumen HTML</td>
</tr>
<tr>
<td></td>
<td>&lt;i&gt;</td>
<td>Mendefinisikan sebuah bagian dari teks dengan suara alternatif atau suasana hati</td>
</tr>
<tr>
<td></td>
<td>&lt;iframe&gt;</td>
<td>Mendefinisikan sebuah frame inline</td>
</tr>
<tr>
<td></td>
<td>&lt;img&gt;</td>
<td>Mendefinisikan sebuah image</td>
</tr>
<tr>
<td></td>
<td>&lt;input&gt;</td>
<td>Mendefinisikan sebuah input control</td>
</tr>
<tr>
<td></td>
<td>&lt;ins&gt;</td>
<td>Mendefinisikan teks yang telah dimasukkan ke dalam dokumen</td>
</tr>
<tr>
<td></td>
<td>&lt;kbd&gt;</td>
<td>Mendefinisikan input keyboard</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;keygen&gt;</td>
<td>Mendefinisikan sebuah key-pair generator field (for forms)</td>
</tr>
<tr>
<td></td>
<td>&lt;label&gt;</td>
<td>Mendefinisikan sebuah label dari sebuah &lt;input&gt; element</td>
</tr>
<tr>
<td></td>
<td>&gt;&lt;legend&gt;</td>
<td>Mendefinisikan sebuah caption dari sebuah &lt;fieldset&gt; element</td>
</tr>
<tr>
<td></td>
<td>&lt;li&gt;</td>
<td>Mendefinisikan sebuah daftar item</td>
</tr>
<tr>
<td></td>
<td>&lt;link&gt;</td>
<td>Mendefinisikan hubungan antara dokumen dengan sumber daya eksternal (digunakan untuk link ke style sheet)</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;main&gt;</td>
<td>Menentukan konten utama dari dokumen</td>
</tr>
<tr>
<td></td>
<td>&lt;map&gt;</td>
<td>Mendefinisikan client-side image-map</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;mark&gt;</td>
<td>Mendefinisikan ditandai / teks yang disorot</td>
</tr>
<tr>
<td></td>
<td>&lt;menu&gt;</td>
<td>Mendefinisikan daftar / menu perintah</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;menuitem&gt;</td>
<td>Mendefinisikan item perintah / menu bahwa pengguna dapat memanggil dari menu popup</td>
</tr>
<tr>
<td></td>
<td>&lt;meta&gt;</td>
<td>Mendefinisikan metadata tentang sebuah dokumen HTML</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;meter&gt;</td>
<td>Mendefinisikan pengukuran skalar dalam kisaran dikenal (gauge)</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;nav&gt;</td>
<td>Mendefinisikan navigasi untuk links</td>
</tr>
<tr>
<td></td>
<td>&lt;noframes&gt;</td>
<td><span class="deprecated">Tag ini tidak didukung pada HTML5.
</span>Mendefinisikan sebuah konten alternatif untuk pengguna yang tidak mendukung frame</td>
</tr>
<tr>
<td></td>
<td>&lt;noscript&gt;</td>
<td>Mendefinisikan sebuah konten alternatif untuk pengguna yang tidak mendukung script sisi klien</td>
</tr>
<tr>
<td></td>
<td>&lt;object&gt;</td>
<td>Mendefinisikan sebuah embedded objek</td>
</tr>
<tr>
<td></td>
<td>&lt;ol&gt;</td>
<td>Mendefinisikan sebuah daftar tersusun</td>
</tr>
<tr>
<td></td>
<td>&lt;optgroup&gt;</td>
<td>Mendefinisikan sekelompok opsi terkait dalam daftar drop-down</td>
</tr>
<tr>
<td></td>
<td>&lt;option&gt;</td>
<td>Mendefinisikan sebuah option dari sebuah daftar drop-down</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;output&gt;</td>
<td>Mendefinisikan hasil dari sebuah perhitungan</td>
</tr>
<tr>
<td></td>
<td>&lt;p&gt;</td>
<td>Mendefinisikan sebuah paragraph</td>
</tr>
<tr>
<td></td>
<td>&lt;param&gt;</td>
<td>Mendefinisikan sebuah parameter untuk sebuah object</td>
</tr>
<tr>
<td></td>
<td>&lt;pre&gt;</td>
<td>Mendefinisikan teks preformatted</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;progress&gt;</td>
<td>Menggambarkan kemajuan tugas</td>
</tr>
<tr>
<td></td>
<td>&lt;q&gt;</td>
<td>Mendefinisikan sebuah kutipan singkat</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;rp&gt;</td>
<td>Mendefinisikan apa yang harus ditampilkan di browser yang tidak mendukung penjelasan ruby</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;rt&gt;</td>
<td>Mendefinisikan sebuah penjelasan / pengucapan karakter (untuk tipografi Asia Timur)</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;ruby&gt;</td>
<td>Mendefinisikan sebuah penjelasan ruby (untuk tipografi Asia Timur)</td>
</tr>
<tr>
<td></td>
<td>&lt;s&gt;</td>
<td>Mendefinisikan teks yang sudah tidak benar</td>
</tr>
<tr>
<td></td>
<td>&lt;samp&gt;</td>
<td>Mendefinisikan contoh output dari program komputer</td>
</tr>
<tr>
<td></td>
<td>&lt;script&gt;</td>
<td>Mendefinisikan sebuah script dari sisi klien</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;section&gt;</td>
<td>Mendefinisikan sebuah section didalam sebuah dokumen</td>
</tr>
<tr>
<td></td>
<td>&lt;select&gt;</td>
<td>Mendefinisikan sebuah daftar drop-down</td>
</tr>
<tr>
<td></td>
<td>&lt;small&gt;</td>
<td>Mendefinisikan teks kecil</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;source&gt;</td>
<td>Mendefinisikan lebih dari satu sumber media untuk elemen media seperti (&lt;video&gt; dan &lt;audio&gt;)</td>
</tr>
<tr>
<td></td>
<td>&lt;span&gt;</td>
<td>Mendefinisikan sebuah section didalam sebuah dokumen</td>
</tr>
<tr>
<td></td>
<td>&lt;strike&gt;</td>
<td><span class="deprecated">Tag ini tidak didukung pada HTML5. Gunakan &lt;del&gt; atau &lt;s&gt; sebagai penggantinya pada HTML5.
</span>Mendefinisikan teks strikethrough</td>
</tr>
<tr>
<td></td>
<td>&lt;strong&gt;</td>
<td>Mendefinisikan teks yang penting</td>
</tr>
<tr>
<td></td>
<td>&lt;style&gt;</td>
<td>Mendefinisikan informasi style untuk sebuah dokumen</td>
</tr>
<tr>
<td></td>
<td>&lt;sub&gt;</td>
<td>Mendefinisikan teks subscripted</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;summary&gt;</td>
<td>Mendefinisikan sebuah judul yang terlihat dalam sebuah &lt;details&gt; element</td>
</tr>
<tr>
<td></td>
<td>&lt;sup&gt;</td>
<td>Mendefinisikan teks superscripted</td>
</tr>
<tr>
<td></td>
<td>&lt;tabel&gt;</td>
<td>Mendefinisikan sebuah tabel</td>
</tr>
<tr>
<td></td>
<td>&lt;tbody&gt;</td>
<td>Grup isi tubuh dalam sebuah tabel</td>
</tr>
<tr>
<td></td>
<td>&lt;td&gt;</td>
<td>Mendefinisikan sebuah cell didalam sebuah tabel</td>
</tr>
<tr>
<td></td>
<td>&lt;teksarea&gt;</td>
<td>Mendefinisikan sebuah input control multiline atau teks area</td>
</tr>
<tr>
<td></td>
<td>&lt;tfoot&gt;</td>
<td>Grup isi footer dalam sebuah tabel</td>
</tr>
<tr>
<td></td>
<td>&lt;th&gt;</td>
<td>Mendefinisikan sebuah header cell didalam sebuah tabel</td>
</tr>
<tr>
<td></td>
<td>&lt;thead&gt;</td>
<td>Grup header dalam sebuah tabel</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;time&gt;</td>
<td>Mendefinisikan sebuah tanggal atau waktu</td>
</tr>
<tr>
<td></td>
<td>&lt;title&gt;</td>
<td>Mendefinisikan sebuah title untuk dokumen</td>
</tr>
<tr>
<td></td>
<td>&lt;tr&gt;</td>
<td>Mendefinisikan sebuah row didalam sebuah tabel</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;track&gt;</td>
<td>Mendefinisikan trek teks untuk elemen media yang (&lt;video&gt; dan &lt;audio&gt;)</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td>&lt;tt&gt;</td>
<td><span class="deprecated">Tag ini tidak didukung pada HTML5. Gunakan CSS sebagai penggantinya pada HTML5.
</span>Mendefinisikan teletype teks</td>
</tr>
<tr>
<td></td>
<td>&lt;u&gt;</td>
<td>Mendefinisikan teks yang harus berbeda dari teks biasa</td>
</tr>
<tr>
<td></td>
<td>&lt;ul&gt;</td>
<td>Mendefinisikan sebuah unordered list</td>
</tr>
<tr>
<td></td>
<td>&lt;var&gt;</td>
<td>Mendefinisikan sebuah variable</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;video&gt;</td>
<td>Mendefinisikan sebuah video atau movie</td>
</tr>
<tr>
<td><img src="https://static.cdn-cdpl.com/wp-images/2016/06/html5_badge20.png" alt="Logo HTML5" width="25" height="25" /></td>
<td class="html5badge">&lt;wbr&gt;</td>
<td>Mendefinisikan sebuah kemungkinan line-break</td>
</tr>
</tbody>
</table>