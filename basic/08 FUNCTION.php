<!--Function untuk efisiensi program-->
<!-- Created By Abdul Haris Dwi Utomo 2018 -->
<!--
	Fungsi ini merupakan sekumpulan perintah program, bisa berupa argumen if,pengulangan,variable
	kemudian mengolah data tersebut sesuai dengan kebutuhan
	
	Bentuk umum function seperti ini

	function namafungsi(parameter1, parameter2, ...parameter n)
	{
		pernyataan berupa perintah yang beriisi program;
	}
	-->
<?php
//pertama kita definisikan dulu nama fungsinya
//kemudian mengisi fungsi tersebut dengan perintah
function testing()
{
	echo "ini adalah contoh function";
}
//untuk menggunakan fungsi yang sudah di buat kita perlu memanggilnya
testing();
?>

<!-- contoh lain -->
<?php
function testing2($nilai)
{
	echo "Tinggi badan anda adalah $nilai";
}

$tinggibadan=170;
testing($tinggibadan);
?>

<!-- contoh lain menggunakan parameter -->
<?php
function penjumlahan($nilai1,$nilai2)
{
	$total=$nilai1+$nilai2;
	echo "$nilai1 + $nilai2 =".$total;
}
penjumlahan (30,20);
?>
<!--contoh lain dengan mengembalikan nilai fungsi-->

<?php
function jumlah($nilai1,$nilai2)
{
	$total=$nilai1+$nilai2;
	return $total;
}

$jumlah30plus20=jumlah(30,20);
echo $jumlah30plus20;
?>