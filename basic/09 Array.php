<!--Array-->
<!-- Created By Abdul Haris Dwi Utomo 2018 -->
<!--
	array merupakan suatu kumpulan data variable yang tersusun secara index

	contoh syntax array adalah
	array(elemen)
	-->
<?php
$tahun=array(1991,1995,1998,2005,2008);
//index 0 memiliki nilai 1991
//index 1 memiliki nilai 1995
//index 2 memiliki nilai 1998
//index 3 memiliki nilai 2005
//index 4 memiliki nilai 2008

//jadi $tahun[3] memiliki nilai 2005
echo $tahun[3];
?>

<!--
	PHP array dapat memuat berbagai varibale data dengan tipe data lain
-->

<?php
$mahasiswa=array("DWI","Laki-Laki","24/02/1991","B",3.41,"Bot Technology");
for($x=0;$x<6;$x++)
{
	echo $mahasiswa[$x]."<br/>";
}


$mahasiswa=array("DWI","Laki-Laki","24/02/1991","B",3.41,"Bot Technology");
end($mahasiswa);
for($x=0;$x<6;$x++)
{
	echo "index array mahasiswa".key($mahasiswa)." berisi ".current($mahasiswa)."<br/>";
	prev($mahasiswa);

}
$transport = array('foot', 'bike', 'car', 'plane');

$mode = current($transport); // $mode = 'foot';
$mode = next($transport);    // $mode = 'bike';
$mode = current($transport); // $mode = 'bike';
$mode = prev($transport);    // $mode = 'foot';
$mode = end($transport);     // $mode = 'plane';
$mode = current($transport); // $mode = 'plane';

count($transport);//menghitung jumlah array


//menggunakan foreach
$mahasiswa=array("DWI","Laki-Laki","24/02/1991","B",3.41,"Bot Technology");
foreach ($mahasiswa as $datamahasiswa) {
	echo $datamahasiswa."<br/>";
}
?>

<!--

	Penjelasan array masih cukup luas
	--mengakses array dalam variable terpsah menggunakan list($nama,$jeniskelamin,$tgllahir,$poin,$ip,$spesialisasi)=$mahasiswa
	--mengurutkan array
	--multidimensional array 
	--dll
-->
