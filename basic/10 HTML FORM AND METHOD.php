<!--HTML METHOD DAN FORM-->
<!-- Created By Abdul Haris Dwi Utomo 2018 -->
<!--
	Materi ini sangat penting di dunia pemograman
	Karena dengan form ini website kita jadi lebih interaktif seperti contoh mengisi polling berkomentar pada suatu artikel
	form sendiri terdapat 2 method yaitu POST dan GET

	1. Buat Form dan simpan dengan nama form1.html dan diisi dengan sebuah form dengan method POST

-->
<html>
<head><title>Form 1 - Latihan Form</title></head>
<body>
<!--
// disini kita akan manfaatkan table untuk membuat form
// pertama buat formnya terlebih dahulu
-->
<form method="POST" action="process1.php">
	<table border='1' width='10%' cellpading='1' cellspacing='1' align='center'>
		<!--
			kita membutuhkan satu input bertipe text, input ini nantinya yang
			berperan untuk menerima inputan dari user
		-->
	<tr>
		<td>Name</td>
		<td><input type="text" name="nama" /></td>
	</tr>

	<!--
	// kemudian kita membutuhkan input bertipe submit yang berfungsi untuk
	// mengirimkan datanya ke file action,
	// dalam hal ini process1.php
	-->

	<tr>
		<td></td>
		<td><input type="submit" name="kirim" value = "kirim" /></td>
	</tr>
	</table>
<!--
kemudian jangan lupa untuk menutup formnya
-->
</form>
</body>
</html>

<!--
BUAT PROSES FORMNYA simpan dengan nama file process1.php
-->
<?php
//untuk mengambil varible yang di kirim dari form dengan metode post dengan cara seperti ini.
echo $_POST['nama'];
?>

<!-- 
	bagaimana dengan metode get?
	hal tersebut sama saja tinggal mengganti method="POST" menjadi method="GET"

	dan penangkapan parameter dengan mehode get adalah dengan cara seperti dibawha ini:
-->
<?php
echo $_GET['nama'];
?>
