<!--IF ELSE-->
<!-- Created By Abdul Haris Dwi Utomo 2018 -->
<!-- Argument if sering di pakai oleh programmer PHP alasannya adalah mudah dan sederhana-->
<!-- dalam dunia nyata  kondisi ini adalah sebuah pengandaian dengan contoh-->
<!-- Jika Cuaca Cerah Maka Saya Akan Berangkat Kuliah-->
<!-- Cara Penulisannya adalah

	if kriteria {
	
	} 

-->
<?php
$cuaca="cerah";
if($cuaca=="cerah")
{
	echo "Saya Akan Berangkat Kuliah";
}
?>

<?php
$jarak=40;
if($jarak <= 40)
{
	echo "Jalan Kaki Saja";
}
if($jarak >=40)
{
	echo "Naik Motor";
}
if($jarak !=40)
{
	echo "Diam ditempat";
}


//jika kita menggabungkan if dan else kita akan mendapatkan dua statement contoh:

// jika cuaca cerah 
// maka saya akan berangkat kuliah

// jika tidak
// maka saya akan buat mie ramen

$cuaca="mendung";
if($cuaca == "cerah") // jika cuaca cerah
{
	echo "Saya akan berangkat kuliah";
}else{
	echo "Saya akan buat mie ramen";
}

$cuaca="mendung";

if($cuaca != "mendung")// jika cuaca tidak cerah
{
	echo "Saya akan Berkangkat Kuliah";
}else{
	echo "Saya akan buat mie ramen";
}


?>
<!--
Menggunakan argumen if,else if dan else
contoh kondisi perandaian :

Jika mendung
Saya bawa payung

Jika Cerah
Maka Saya akan berangkat kuliah meskipun jaraknya 20km

Jika Banjir
Maka saya akan bawa perahu dari rumah

dan seterusnya
-->
<?php
$nilai='A';
if($nilai=='A')
{
	echo "Baguss.. Baguss";
}
else if($nilai == 'B')
{
	echo "Baik...";
}
else if($nilai == 'C')
{
	echo "Cukup...";
}
else if($nilai == 'D')
{
	echo "Kurang...";
}
//dan seterusnya
?>
<!--
SWITCH sama seperti IF dan ELSE perbedaannya hanya pada penulisan syntaxnya
kalo if else menggunakan {} kalo switch memberhentikan keputusan dengan fungsi break;
-->
<?php
$nilai='A';
switch($nilai)
{
	case "A":
	echo "Baguss.. Baguss";
	break;

	case "B":
	echo "Baik..";
	break;

	case "C":
	echo "Cukup..";
	break;

	case "D":
	echo "Kurang..";
	break;
}

//perbedaan dengan if else switch hanya dapat di gunakan oleh 1 variable saja klo if else bisa menggunakan varibale yang berbeda beda
?>