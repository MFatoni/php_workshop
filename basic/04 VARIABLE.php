<!--variable-->
<!-- Created By Abdul Haris Dwi Utomo 2018 -->
<html>
<head>
	<title>Latihan Variable</title>
</head>
<body>
	<?php
	//contoh variable
	$tahun=2017; //ini adalah proses assignment
	echo $tahun;

	$tahun=2018;
	echo $tahun;
	//tipe variable ada 3 integer,float dan string
	//Integer adalah bilangan bulat 0,1,2,3,4 dan seterusnya
	//Float adalah bilangan desimal 1,2 1,3 ,1,4 dan seterusnya
	//String adalah tipe data yang berisi karakter,angka,huruf,simbol misalkan "Jl Margonda Raya No 20"


	// syarat variable
	// 	harus di awali oleh huruf atau _, tidak boleh memuat spasi
	// tidak boleh memuat spesial karakter (~!@#$% dan lain lain)
	?>
</body>
</html>
