<!--STRUKTUR DAN SYNYTAX MYSQL-->
<!--Created By Abdul Haris Dwi UTomo @2018-->

Tipe Data String
- char, memiliki panjang 255
- varchar, memiliki panjang 255
- tinytext, memiliki panjang 255
- text, tidak terbatas
- mediumtext, 1 juta
- longtext, 4 Milyar

Tipe Data Numerik
- int, -2 M sampai dengan 4 M
- tinyint, -128 sampai dengan 255
- mediumint, -8 Juta sampai dengan 8 Juta
- bigint, -92 Triliyun sampai dengan 92 Triliyun
- float, bilangan desimal positif, bilangan real, memiliki koma-komaan ...
- double, bilangan desimal negatif dan positif, dan memiliki koma-komaan ...

Tipe Data Date dan TIme
- date, hanya memuat tanggal saja, dengan formatnya seperti ini tahun-bulan-tanggal
- time, hanya memuat waktu saja, formatnya HH:MM:SS (Jam:Menit:Detik)
- datetime, memuat tanggal dan waktu, formatnya Tahun-Bulan-Hari Jam:Menit:Detik
- year,

Tipe Key
- Primary Key, Kunci yang menghubungkan antara 1 table dengan table lainnya

Auto Increment
- Auto Increment menyebabkan pertambahan nilai urutan secara otomatis, suatu record
memiliki nilai 1, kemudian jika ada record yang lain lagi di insertkan ke dalam database,
maka urutannya akan menjadi 2 dan begitu seterus-seterusnya ....

Pada bagian ini akan saya akan coba jelaskan beberapa syntax MySQL yang sering
digunakan dalam membuat website, maupun aplikasi-aplikasi berbasis website, baik
dalam melakukan pembuatan database, penambahanisi database, untuk menghapus isi
database, ataupun untuk menghapus database itu sendiri. Syntax yang kita gunakan
adalah :

1. create
2. use
3. show
4. drop
5. alter
6. select
7. insert
8. update
9. delete

