<!--STRUKTUR DAN SYNYTAX MYSQL-->
<!--Created By Abdul Haris Dwi UTomo @2018-->
1. Create

ini digunakan untuk membuat database maupun table. bahwa RDBMS(Relational Database Management System) itu sistem
database yang berisi informasi dalam bentuk table-table yang saling berkaitan. Jadi
langkah awal dalam membuat database adalah menggunakan fungsi create, syntaxnya
seperti ini :

============================
create database namadatabase;
============================
ex: create database ecommerce

untuk membuat tablenya

================================================
create table namatable (field1(ukuran), field2(ukuran), ...)
=================================================
ex: create table barang (idBarang int(5) primary key auto_increment, nama_barang varchar(50), qty int(11), harga double(30,0));


2. Use
use digunakan untuk menggunakan database yang telah dibuat yang nanntinya database ini akan di edit atau di modifikasi, syntaxnya
==============================
use namadatabase;
=============================
ex : use ecommerce

3. Drop
drop diguakan untuk menghapppus database ataupun table yang ada, syntaxnya adalah:
===============================
Drop database namadatabase;
===============================

untuk menghapus table gunakan
===============================
Drop table namatable
===============================

4. Alter
Alter berfungsi untuk memodifikasi
table yang telah di buat, modifikasi nya seperti menambahhkan field, mengganti size dari
suatu field, menghapus field, dan mengganti nama field.

sebagai contoh merubah nama kolom
=================================
alter table namatable change namafieldlama namafieldbaru typedata
=================================
ex: alter table barang change nama_barang namaBarang varchar(75);


5. SELECT
Di gunakan untuk menampilkan isi dari suatu table, bisa dengan kriteria tertentu bisa juga
dapat menampilkan keseluruhan tanpa adanya kriteria. Penggunaan standarnya seperti
ini:
=========================
SELECT * FROM nama_table
=========================

dengan kriteria
=============================================
select * from namatable where field=kriteria
=============================================

untuk menampilkan nilai terbanyak
=====================================
select max(field) from namatable
=====================================

untuk menjumlah isi record salah satu field
===================================
select sum(field) from namatable
===================================

untuk menghitung jumlah record
===================================
select count(*) from namatable
===================================


6. INSERT
insert ini digunakan untuk mengisi record suatu table, syntaxnya seperti ini
====================================
Insert into nama_table(field1,field2,....) values(nilai1,nilai2,....)
====================================
ex : insert into barang(namaBarang,qty,harga) values('TV','5','100000');

7. UPDATE
digunakan untuk memperbaharui isi dari suatu record syntaxnya seperti ini:
===========================================
update nama_table set field=nilaibaru where field=kriteria
===========================================
ex: update barang set namaBarang='TV LCD' where idBarang=1;

8. DELETE
Menghapus Recod dari suatu table penggunaannya seperti ini
============================================
delete from namatable where field=kriteria
============================================
ex : delete from barang where idBarang=1