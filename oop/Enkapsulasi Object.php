<!--Enkapsulasi Object-->
<!--Created By Abdul Haris Dwi Utomo-->
<!--
	Dengan enkapsulasi, kita bisa membuat pembatasan akses kepada property dan method, sehingga hanya property dan method tertentu saja yang bisa diakses dari luar class. Enkapsulasi juga dikenal dengan istilah ‘information hiding’. ada 3 jenis enkapsulasi yang bisa anda manfaatkan yaitu Public, Private Protected.
	-->

<!--contoh public-->

<?php 
	class person {
		public $name; 
		function set_name($new_name) { 
			$this->name = $new_name;  
 		}
 
   		function get_name() {
			return $this->name;
		}
	} 
?>

<?php
$person1 = new Person();
// properti bisa di akses secara langsung
echo "Hai ".$person1->name='Mark Rujakbebeg';
echo "<hr>";
// method bisa di akses secara langsung
echo $person1->get_name();
?>

<!--contoh private-->

<?php 
	class person {
		private $name; 
		function set_name($new_name) { 
			$this->name = $new_name;  
 		}
 
   		function get_name() {
			return $this->name;
		}
	} 
?>

<?php
$person1 = new Person();
// properti bisa di akses secara langsung
echo "Hai ".$person1->name='Mark Rujakbebeg';
echo "<hr>";
// methob tidak bisa di akses secara langsung
echo $person1->get_name();
?>

<!-- contoh protected-->
<?php 
	class person {
		protected $name; 
		function set_name($new_name) { 
			$this->name = $new_name;  
 		}
 
   		function get_name() {
			return $this->name;
		}
	} 
?>

<?php
$person1 = new Person();
// set value dari properti name
$person1->set_name('Mark Rujakbebeg');
// akses value dari properti name
echo $person1->get_name();
// properti tidak bisa di akses secara langsung, kana muncul error
echo "Hai ".$person1->name='Mark Rujakbebeg';
echo "<hr>";
//perbedaan utama antara private dan protected hanyalah jika protected tidak bisa diakses kecuali dari dalam class itu sendiri.
?>