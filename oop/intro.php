<!-- Created Abdul Haris Dwi Utomo 2018-->
<!-- OOP(Object Oriented Programming)-->

<!--Pemrograman berorientasi objek adalah gaya mengkode yang memungkinkan pengembang untuk mengelompokkan tugas-tugas serupa ke dalam kelas.-->
<!--
Pengenalan Object dan Class

Mudahnya bayangkan Class adalah sebuah design/gambar rumah milik seorang arsitek 
dan kayu, kawat, dan beton adalah sebuat object yang akan di gunakan untuk membangun sebuah rumah

Contoh:
-->
<?php
// class Person
// {

// }

// $obj= new Person();
// var_dump($obj);
?>

<!--menambahkan property kedalam class-->
<?php
 
// class Person
// {
//   public $name = "Nama Saya Haris Property!";
//   //public disini mengidentifikasikan visibilitasnya
// }
 
// $obj = new MyClass;
 
// var_dump($obj);

// //untuk membaca property dan output ke browser referensikan object + nama property yang akan di baca
// echo $obj->name; // Output the property
 
?>

<!-- MENDEFINISIKAN METHOD CLASS
method adalah fungsi2 yang terdapat pada class yang dapat di kerjakan oleh suatu object-->

<?php
 
// class Person
// {
//   public $name = "Nama Saya Haris Property!";
 
//   public function setProperty($newval)
//   {
//       $this->name = $newval;
//   }
 
//   public function getProperty()
//   {
//       return $this->name . "<br />";
//   }
// }
// //simpan class ini menjadi class_lib.php 
// $obj = new Person;
 
// echo $obj->getProperty(); // Get the property value
 
// $obj->setProperty("Saya Haris Property yang baru!"); // Set a new one
 
// echo $obj->getProperty(); // Read it out again to show the change
 
?>

<!-- Instance Object/Membuat Object dengan Include Class-->

<?php //include("class_lib.php"); ?>
<?php 
	// $stefan = new person();
	// $jimmy = new person;
 
	// $stefan->setProperty("Mark Rujakbebek");
	// $jimmy->setProperty("Steve need jobs");
 
	// echo "Mark's full name: " . $stefan->getProperty();
	// echo "Steve's full name: " . $jimmy->getProperty(); 
?>